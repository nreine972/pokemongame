export const toggleAsideInfo = ({ enabled, id }) => ({
  type: "TOGGLE_ASIDE_DETAIL",
  payload: { enabled, id },
});

export const changeAsideInfo = ({ id }) => ({
  type: "CHANGE_ASIDE_DETAIL",
  payload: { id },
});

export const switchMode = ({ mode }) => ({
  type: "SWITCH_MODE",
  payload: { mode },
});

export const addInTeam = ({ pokemon, asideEnabled }) => ({
  type: "ADD_IN_TEAM",
  payload: { pokemon, asideEnabled },
});

export const addSelectedAttacks = ({ attack, category }) => ({
  type: "ADD_SELECTED_ATTACK",
  payload: { attack, category },
});

export const removeSelectedAttacks = ({ attack }) => ({
  type: "REMOVE_SELECTED_ATTACK",
  payload: { attack },
});

export const createMatch = () => ({
  type: "CREATE_MATCH",
});

export const rematch = () => ({
  type: "REMATCH",
});

export const addPokemons = ({ pokemons }) => ({
  type: "ADD_POKEMONS",
  payload: { pokemons },
});

export const launchAttack = ({ attack, attacker, receiver, turn }) => ({
  type: "LAUNCH_ATTACK",
  payload: { attack, attacker, receiver, turn },
});
