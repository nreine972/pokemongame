import { gql } from "@apollo/client";

export const getPokemons = gql`
  query getPokemons {
    pokemons(first: 200) {
      id
      number
      name
      image
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      classification
      types
      resistant
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
      weaknesses
      fleeRate
      maxCP
      evolutions {
        id
        image
        name
      }
      maxHP
    }
  }
`;
