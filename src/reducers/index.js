const initState = {
  constants: {
    teamMinPokemonNb: 3,
    teamMaxPokemonNb: 6,
    maxAttacks: 4,
  },
  mode: "home",
  ui: {
    aside: {
      enabled: false,
    },
    battle: {
      enabled: false,
    },
    finalBoard: {
      enabled: false,
    },
    actionBar: {
      enabled: false,
    },
  },
  asidePokemonData: {},
  pokemons: [],
  battle: {
    turn: "user",
    computer: {
      pokemons: [],
      active: {},
    },
    user: {
      pokemons: [],
      active: {},
    },
    lastAttack: {},
    highlights: [],
  },
};

function createMatch({ battle, pokemons, ui }) {
  const userTeam = battle.user.pokemons;
  const computerTeam = generateComputerTeam({ userTeam, pokemons });
  const computerActive = computerTeam[0];
  const userActive = battle.user.pokemons[0];
  computerActive.health = 100;
  userActive.health = 90;

  const output = {
    mode: "battle",
    ui: {
      ...ui,
      battle: {
        enabled: true,
      },
    },
    battle: {
      ...battle,
      user: {
        ...battle.user,
        active: {
          ...userActive,
          health: 100,
        },
      },
      computer: {
        pokemons: computerTeam,
        active: {
          ...computerActive,
          health: 100,
        },
      },
    },
  };

  console.log("active pokemon", battle.user.pokemons[0]);

  return output;
}

function resetBattle({ battle }) {
  const output = {
    battle: {
      ...battle,
      user: {
        ...battle.user,
        active: {
          ...battle.user.active,
          health: 100,
        },
      },
      computer: {
        ...battle.user,
        active: {
          ...battle.computer.active,
          health: 100,
        },
      },
    },
  };

  return output;
}

function generateComputerTeam({ userTeam, pokemons }) {
  const computerTeam = [];
  const pokemonIds = userTeam.map((pokemon) => pokemon.id);
  /* Rules : 
    For each user pokemon, find an opponent with close value of Max HP
  */
  for (const userPokemon of userTeam) {
    for (let i = 1; i < 3; i++) {
      const result = pokemons.find((pokemon) => {
        const lowLimit = Math.floor(
          userPokemon.maxHP - (userPokemon.maxHP * (i * 10)) / 100
        );
        const highLimit = Math.floor(
          userPokemon.maxHP + (userPokemon.maxHP * (i * 10)) / 100
        );
        return (
          lowLimit < pokemon.maxHP &&
          pokemon.maxHP < highLimit &&
          pokemonIds.indexOf(pokemon.id) === -1
        );
      });

      if (result) {
        computerTeam.push(result);
        break;
      }
    }
  }

  return computerTeam;
}

function removeSelectedAttacks({ asidePokemonData, attack }) {
  const selectedAttacks = [...asidePokemonData.selectedAttacks].filter(
    (item) => item.name !== attack.name
  );

  return selectedAttacks;
}

function addSelectedAttacks({ asidePokemonData, attack, category }) {
  const selectedAttacks = [
    ...asidePokemonData.selectedAttacks,
    {
      ...attack,
      category,
    },
  ];

  return selectedAttacks;
}

function changeAsidePokemon({ id, pokemons }) {
  const asidePokemon = pokemons.find((pokemon) => pokemon.id === id);
  return asidePokemon;
}

function switchMode({ mode, asidePokemonData, ui, battle }) {
  return {
    mode,
    asidePokemonData:
      mode === "home" ? {} : { ...asidePokemonData, selectedAttacks: [] },
    ui:
      mode === "home"
        ? {
            battle: { enabled: false },
            aside: { enabled: false },
            finalBoard: { enabled: false },
          }
        : { ...ui },
    battle:
      mode === "home"
        ? {
            computer: {
              pokemons: [],
              active: {},
            },
            user: {
              pokemons: [],
              active: {},
            },
          }
        : { ...battle },
  };
}

function toggleAside({ ui, enabled, id, mode, pokemons }) {
  var output = {};

  if (enabled) {
    const asideData = pokemons.find((pokemon) => {
      return pokemon.id === id;
    });

    output = {
      ui: {
        ...ui,
        aside: {
          enabled,
        },
      },
      asidePokemonData: { ...asideData },
    };

    if (mode === "teambuilder") {
      output.asidePokemonData.selectedAttacks = [];
    }
  } else {
    output = {
      ui: {
        ...ui,
        aside: {
          enabled,
        },
      },
    };
  }

  return output;
}

function attackResult({ attack, attacker, receiver }) {
  const attackerCP = attacker.maxCP;
  const receiverCP = receiver.maxCP;
  const height = Math.ceil(Number(attacker.height.maximum.replace("m", "")));
  const weight = Math.ceil(Number(attacker.weight.maximum.replace("kg", "")));
  const sizeRate = height * weight;
  var attackPower = Math.ceil(
    sizeRate * attack.damage + (10 * attackerCP) / 100
  );

  if (receiver.weaknesses.indexOf(attack.type) !== -1) {
    attackPower += (attackPower * 50) / 100;
  } else if (receiver.resistant.indexOf(attack.type) !== -1) {
    attackPower -= (attackPower * 50) / 100;
  }

  const damage = (attackPower * 100) / receiverCP;

  var newHealth = receiver.health - damage;

  if (newHealth < 0) {
    newHealth = 0;
  }

  console.log("height", height);
  console.log("weight", weight);
  console.log("userPokemonCP", attackerCP);
  console.log("sizeRate", sizeRate);
  console.log("attackPower", attackPower);
  console.log("attackPower", attackPower);
  console.log("newHealth", newHealth);

  return newHealth;
}

function updateHealth({ attack, attacker, receiver, turn, battle, ui }) {
  console.log("battle ", battle);
  const output = {
    ui: {
      ...ui,
    },
    battle: {
      ...battle,
      highlights: [...battle.highlights],
    },
  };

  const newHealth = attackResult({ attack, attacker, receiver });

  if (turn === "user") {
    output.battle["computer"].active.health = newHealth;
  } else {
    output.battle["user"].active.health = newHealth;
  }

  if (newHealth === 0) {
    output.ui.battle.enabled = false;
    output.ui.finalBoard.enabled = true;
    output.mode = "finalBoard";
  }

  console.log("attacker image", attacker.image);

  const highlight = {
    attacker: turn,
    attackerName: attacker.name,
    attackerImage: attacker.image,
    receiverName: receiver.name,
    attack: attack.name,
  };

  //output.battle.highlights.push(highlight);
  output.battle.lastAttack = highlight;

  return output;
}

const reducer = (state = initState, action) => {
  console.log("state battle", state.battle);
  const payload = action.payload;
  switch (action.type) {
    case "LAUNCH_ATTACK":
      return {
        ...state,
        ...updateHealth({
          attack: payload.attack,
          attacker: payload.attacker,
          receiver: payload.receiver,
          turn: payload.turn,
          battle: state.battle,
          ui: state.ui,
        }),
      };

    case "TOGGLE_ASIDE_DETAIL":
      return {
        ...state,
        ...toggleAside({
          ui: state.ui,
          enabled: payload.enabled,
          id: payload.id,
          mode: state.mode,
          pokemons: state.pokemons,
        }),
      };
    case "CHANGE_ASIDE_DETAIL":
      return {
        ...state,
        asidePokemonData: changeAsidePokemon({
          id: payload.id,
          pokemons: [...state.pokemons],
        }),
      };

    case "SWITCH_MODE":
      return {
        ...state,
        battle: {
          ...state.battle,
          user: {
            pokemons: [],
          },
        },
        ...switchMode({
          mode: payload.mode,
          asidePokemonData: state.asidePokemonData,
          ui: state.ui,
          battle: state.battle,
        }),
      };

    case "ADD_SELECTED_ATTACK":
      return {
        ...state,
        asidePokemonData: {
          ...state.asidePokemonData,
          selectedAttacks: addSelectedAttacks({
            asidePokemonData: state.asidePokemonData,
            attack: payload.attack,
            category: payload.category,
          }),
        },
      };

    case "REMOVE_SELECTED_ATTACK":
      return {
        ...state,
        asidePokemonData: {
          ...state.asidePokemonData,
          selectedAttacks: removeSelectedAttacks({
            asidePokemonData: state.asidePokemonData,
            attack: payload.attack,
          }),
        },
      };

    case "ADD_IN_TEAM":
      return {
        ...state,
        ui: {
          ...state.ui,
          aside: {
            enabled: payload.asideEnabled,
          },
        },
        battle: {
          ...state.battle,
          user: {
            pokemons: [...state.battle.user.pokemons, payload.pokemon],
          },
        },
      };

    case "CREATE_MATCH":
      return {
        ...state,
        ...createMatch({
          battle: state.battle,
          pokemons: state.pokemons,
          ui: state.ui,
        }),
      };

    case "REMATCH":
      return {
        ...state,
        mode: "battle",
        ui: {
          ...state.ui,
          finalBoard: {
            enabled: false,
          },
          battle: {
            enabled: true,
          },
        },
        ...resetBattle({ battle: state.battle }),
      };

    case "ADD_POKEMONS":
      return {
        ...state,
        pokemons: payload.pokemons,
      };

    default:
      return state;
  }
};

export default reducer;
