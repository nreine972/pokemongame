import React from "react";
import Thumbnails from "./../Thumbnails";
import {
  details,
  detailsTitle,
  detailsList,
  detailsListItem,
  detailsLabel,
} from "./details.scss";

export default function Details({ data }) {
  const createCharacteristics = (data) => {
    let characteristics = [];
    const keys = Object.keys(data);
    const excludeCharacteristics = [
      "id",
      "number",
      "evolutions",
      "image",
      "__typename",
    ];
    characteristics = keys.map((characteristic) => {
      if (excludeCharacteristics.indexOf(characteristic) > -1) {
        return;
      } else if (typeof data[characteristic] === "string") {
        return (
          data.classification && (
            <li key={`${name}-${characteristic}`} className={detailsListItem}>
              <span className={detailsLabel}>{`${characteristic} : `}</span>
              {data[characteristic]}
            </li>
          )
        );
      } else if (Array.isArray(data[characteristic])) {
        return (
          data.classification && (
            <li key={`${name}-${characteristic}`} className={detailsListItem}>
              <span className={detailsLabel}>{`${characteristic} : `}</span>
              {data[characteristic].join(", ")}
            </li>
          )
        );
      }
    });
    return characteristics;
  };

  return (
    <div className={details}>
      <h3 className={detailsTitle}>Characteristics</h3>
      <ul className={detailsList}>
        {createCharacteristics(data).map((characteristic) => characteristic)}
      </ul>
      {data.evolutions && (
        <div>
          <h3 className={detailsTitle}>Evolutions</h3>
          <Thumbnails list={data.evolutions} />
        </div>
      )}
    </div>
  );
}
