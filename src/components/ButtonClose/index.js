import React from "react";
import { connect } from "react-redux";
import { buttonClose, buttonCloseContainer } from "./buttonClose.scss";
import { toggleAsideInfo } from "./../../actions";

function ButtonClose({ toggleAsideInfo }) {
  const closeHandler = () => {
    toggleAsideInfo({ enabled: false, id: null });
  };

  return (
    <div className={buttonCloseContainer}>
      <button onClick={closeHandler} className={buttonClose} />
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    toggleAsideInfo: (payload) => dispatch(toggleAsideInfo(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ButtonClose);
