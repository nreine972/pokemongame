import React from "react";
import { connect } from "react-redux";
import {
  finalBoardClose,
  finalBoardOpen,
  finalBoardWrapper,
  finalBoardTitle,
  finalBoardResult,
  finalBoardImg,
  finalBoardDesc,
  finalBoardPokemon,
  finalBoardAttack,
} from "./finalBoard.scss";

function FinalBoard({ ui, battle }) {
  return (
    <div>
      {battle.lastAttack && (
        <div
          className={ui.finalBoard.enabled ? finalBoardOpen : finalBoardClose}
        >
          <div className={finalBoardWrapper}>
            <h3 className={finalBoardTitle}>
              {battle.lastAttack.attacker === "user" ? "You" : "Computer"} win
              the match !
            </h3>
            <div className={finalBoardResult}>
              <img
                className={finalBoardImg}
                alt=""
                src={battle.lastAttack.attackerImage}
              />
              <p className={finalBoardDesc}>
                <span className={finalBoardPokemon}>
                  {battle.lastAttack.attackerName}
                </span>{" "}
                beat {battle.lastAttack.receiverName} <br /> with{" "}
                <span className={finalBoardAttack}>
                  {battle.lastAttack.attack}
                </span>
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(FinalBoard);
