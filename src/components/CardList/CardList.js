import React from "react";
import { connect } from "react-redux";
import {
  cardlist,
  cardlistTitle,
  cardlistLoading,
  cardlistLoadingHidden,
} from "./cardlist.scss";
import Card from "../Card";

function Cardlist({ pokemons, mode }) {
  return (
    <div>
      <div
        className={pokemons.length ? cardlistLoadingHidden : cardlistLoading}
      ></div>
      <h2 className={cardlistTitle}>
        {mode === "home" ? "Pokedex" : "Build your team"}
      </h2>
      <div className={cardlist}>
        {!!pokemons.length &&
          pokemons.map((pokemon) => <Card key={pokemon.id} data={pokemon} />)}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });

export default connect(mapStateToProps)(Cardlist);
