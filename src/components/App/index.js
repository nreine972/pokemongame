import React, { useEffect } from "react";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { getPokemons } from "./../../gql";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { render } from "react-dom";
import Cardlist from "../CardList/CardList";
import Aside from "../Aside";
import Search from "../Search";
import Topbar from "./../Topbar";
import Battle from "../Battle";
import ActionBar from "../ActionBar";
import FinalBoard from "../FinalBoard";
import { container } from "./index.scss";
import rootReducer from "./../../reducers";
import { addPokemons } from "./../../actions";

/* if (module.hot) {
  module.hot.accept();
} */

const PokemonApp = () => {
  const store = createStore(rootReducer);

  const client = new ApolloClient({
    uri: "https://graphql-pokemon2.vercel.app/",
    cache: new InMemoryCache(),
  });

  useEffect(() => {
    client
      .query({
        query: getPokemons,
      })
      .then((result) => {
        store.dispatch(addPokemons({ pokemons: result.data.pokemons }));
      });
  }, []);

  return (
    <div className={container}>
      <Provider store={store}>
        <Topbar />
        <Search />
        <Cardlist />
        <Aside />
        <Battle />
        <FinalBoard />
        <ActionBar />
      </Provider>
    </div>
  );
};

render(<PokemonApp />, document.getElementById("pokemon-app"));
