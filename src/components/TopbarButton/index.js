import React from "react";
import { connect } from "react-redux";
import { topbarButton, topbarButtonHidden } from "./topbar-button.scss";
import { switchMode } from "./../../actions";

function TopbarButton({ switchMode, mode }) {
  function clickHandler() {
    switchMode({ mode: "teambuilder" });
  }

  return (
    <button
      className={mode === "home" ? topbarButton : topbarButtonHidden}
      onClick={clickHandler}
    >
      {`${mode === "home" ? "Battle" : ""}`}
    </button>
  );
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    switchMode: (payload) => dispatch(switchMode(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TopbarButton);
