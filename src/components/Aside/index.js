import React from "react";
import { connect } from "react-redux";
import AttackPicker from "../AttackPicker";
import ButtonClose from "../ButtonClose";
import Details from "../Details";
import {
  asideClose,
  asideOpen,
  asideImg,
  asideImgContainer,
  asideTitle,
} from "./aside.scss";

function Aside({ mode, ui, asidePokemonData }) {
  return (
    <div className={ui.aside.enabled ? asideOpen : asideClose}>
      <ButtonClose />
      <h2 className={asideTitle}>{asidePokemonData.name}</h2>
      <div className={asideImgContainer}>
        <img
          src={asidePokemonData.image}
          className={asideImg}
          alt={asidePokemonData.name}
        />
      </div>
      {mode === "home" && <Details data={asidePokemonData} />}
      {mode === "teambuilder" && <AttackPicker data={asidePokemonData} />}
    </div>
  );
}
const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(Aside);
