import React from "react";
import { card, cardSelected, cardImg, cardTitle } from "./card.scss";
import { connect } from "react-redux";
import { toggleAsideInfo } from "../../actions";
import Tag from "./../Tag";

function Card({ data, toggleAsideInfo, battle }) {
  const handleCardClick = (id) => {
    toggleAsideInfo({ enabled: true, id });
  };

  const isCardSelected = () => {
    const isCardSelected = !!battle.user.pokemons.find(
      (pokemon) => pokemon.id === data.id
    );

    return isCardSelected;
  };

  return (
    <button
      onClick={() => {
        handleCardClick(data.id);
      }}
      className={isCardSelected() ? cardSelected : card}
      id={data.id}
    >
      <h3 className={cardTitle}>{data.name}</h3>
      <img className={cardImg} alt={data.name} src={data.image} />
      <ul>
        {data.types &&
          data.types.map((type) => (
            <Tag key={`${data.id}-${type}`} type={type} />
          ))}
      </ul>
    </button>
  );
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    toggleAsideInfo: (payload) => dispatch(toggleAsideInfo(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);
