import React from "react";
import { connect } from "react-redux";
import { actionBar, actionBarHidden, actionBarButton } from "./actionbar.scss";
import {
  switchMode,
  toggleAsideInfo,
  addInTeam,
  createMatch,
  rematch,
} from "./../../actions";

function ActionBar({
  mode,
  switchMode,
  toggleAsideInfo,
  asidePokemonData,
  addInTeam,
  createMatch,
  rematch,
  constants,
  battle,
  ui,
}) {
  const isVisible = (mode) => {
    const acceptedModes = ["teambuilder", "battle", "finalBoard"];
    return acceptedModes.indexOf(mode) !== -1 ? true : false;
  };

  const isCreateMatchButtonDisabled = () => {
    let isDisabled = false;
    if (
      constants.teamMinPokemonNb > battle.user.pokemons.length ||
      constants.teamMaxPokemonNb < battle.user.pokemons.length
    ) {
      isDisabled = true;
    } else {
      isDisabled = false;
    }

    return isDisabled;
  };

  const isAddButtonDisabled = () => {
    const selectedAttacks = asidePokemonData.selectedAttacks || [];
    const selectedAttacksFast = selectedAttacks
      ? selectedAttacks.filter((attack) => attack.category === "fast")
      : [];

    const selectedAttacksSpecial = selectedAttacks
      ? selectedAttacks.filter((attack) => attack.category === "special")
      : [];
    const ids = battle.user.pokemons.map((pokemon) => pokemon.id);

    return ids.indexOf(asidePokemonData.id) === -1 &&
      selectedAttacks.length > 0 &&
      selectedAttacksSpecial.length >= 1 &&
      selectedAttacksFast.length >= 1
      ? false
      : true;
  };
  const isChangePokemonButtonDisabled = () => {
    let disabled = true;
    return disabled;
  };

  const handleChangeClick = () => {};

  const handleRematchClick = () => {
    rematch();
  };

  const handlePrevClick = () => {
    if (ui.aside.enabled) {
      toggleAsideInfo({ enabled: false, id: null });
    } else {
      switchMode({ mode: "home" });
    }
  };

  const handleAddClick = () => {
    addInTeam({ pokemon: asidePokemonData, asideEnabled: false });
  };

  const handleCreateClick = () => {
    createMatch();
  };

  return (
    <div className={isVisible(mode) ? actionBar : actionBarHidden}>
      <button onClick={handlePrevClick} className={actionBarButton}>
        Back
      </button>
      {ui.aside.enabled ? (
        <button
          disabled={isAddButtonDisabled()}
          onClick={handleAddClick}
          className={actionBarButton}
        >
          Add To Team
        </button>
      ) : mode === "teambuilder" ? (
        <button
          disabled={isCreateMatchButtonDisabled()}
          onClick={handleCreateClick}
          className={actionBarButton}
        >
          Create Match
        </button>
      ) : mode === "battle" ? (
        <button
          disabled={isChangePokemonButtonDisabled()}
          onClick={handleChangeClick}
          className={actionBarButton}
        >
          Change Pokemon
        </button>
      ) : mode === "finalBoard" ? (
        <button onClick={handleRematchClick} className={actionBarButton}>
          Rematch
        </button>
      ) : null}
    </div>
  );
}
const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    switchMode: (payload) => dispatch(switchMode(payload)),
    toggleAsideInfo: (payload) => dispatch(toggleAsideInfo(payload)),
    addInTeam: (payload) => dispatch(addInTeam(payload)),
    createMatch: (payload) => dispatch(createMatch(payload)),
    rematch: (payload) => dispatch(rematch(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ActionBar);
