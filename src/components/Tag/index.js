import React from "react";
import styles from "./tag.scss";

export default function Tag({ type }) {
  return <div className={styles[`tag${type}`]}>{type}</div>;
}
