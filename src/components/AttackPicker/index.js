import React from "react";
import { connect } from "react-redux";
import Thumbnails from "./../Thumbnails";
import {
  attackPicker,
  attackPickerTitle,
  attackPickerContainer,
  attackPickerSubContainer,
  attackPickerInputWrapper,
  attackPickerInput,
  attackPickerLabel,
} from "./attackPicker.scss";
import { addSelectedAttacks, removeSelectedAttacks } from "./../../actions";

function AttackPicker({
  battle,
  asidePokemonData,
  addSelectedAttacks,
  removeSelectedAttacks,
  constants,
}) {
  const attackCheckHandler = (attack, category, checked) => {
    if (checked) {
      addSelectedAttacks({ attack, category });
    } else {
      removeSelectedAttacks({ attack });
    }
  };

  const isCheckboxesDisabled = (attackName) => {
    var attackNames = asidePokemonData.selectedAttacks.map(
      (attack) => attack.name
    );
    const condition =
      !!asidePokemonData.selectedAttacks &&
      asidePokemonData.selectedAttacks.length >= constants.maxAttacks &&
      attackNames.indexOf(attackName) === -1;

    return condition;
  };

  return (
    <div className={attackPicker}>
      <h3 className={attackPickerTitle}>Select Attacks</h3>
      <form>
        <div className={attackPickerContainer}>
          <div className={attackPickerContainer}>
            <div className={attackPickerSubContainer}>
              {asidePokemonData &&
                asidePokemonData.attacks &&
                asidePokemonData.attacks.fast &&
                asidePokemonData.attacks.fast.map((attack) => {
                  return (
                    <div
                      key={`${asidePokemonData.id}-${attack.name}`}
                      className={attackPickerInputWrapper}
                    >
                      <input
                        id={`${asidePokemonData.id}-${attack.name}`}
                        type="checkbox"
                        className={attackPickerInput}
                        disabled={isCheckboxesDisabled(attack.name)}
                        onChange={(e) => {
                          attackCheckHandler(attack, "fast", e.target.checked);
                        }}
                      />
                      <label
                        className={attackPickerLabel}
                        htmlFor={`${asidePokemonData.id}-${attack.name}`}
                      >
                        {attack.name}
                      </label>
                    </div>
                  );
                })}
            </div>
            <div className={attackPickerSubContainer}>
              {asidePokemonData &&
                asidePokemonData.attacks &&
                asidePokemonData.attacks.special &&
                asidePokemonData.attacks.special.map((attack) => {
                  return (
                    <div
                      key={`${asidePokemonData.id}-${attack.name}`}
                      className={attackPickerInputWrapper}
                    >
                      <input
                        id={`${asidePokemonData.id}-${attack.name}`}
                        type="checkbox"
                        className={attackPickerInput}
                        disabled={isCheckboxesDisabled(attack.name)}
                        onChange={(e) => {
                          attackCheckHandler(
                            attack,
                            "special",
                            e.target.checked
                          );
                        }}
                      />
                      <label
                        className={attackPickerLabel}
                        htmlFor={`${asidePokemonData.id}-${attack.name}`}
                      >
                        {attack.name}
                      </label>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </form>
      <h3 className={attackPickerTitle}>Current Team</h3>
      <Thumbnails list={battle.user.pokemons} />
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    addSelectedAttacks: (payload) => dispatch(addSelectedAttacks(payload)),
    removeSelectedAttacks: (payload) =>
      dispatch(removeSelectedAttacks(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AttackPicker);
