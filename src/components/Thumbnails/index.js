import React from "react";
import { connect } from "react-redux";
import {
  thumbnails,
  thumbnailListItem,
  thumbnailBtn,
  thumbnailImg,
} from "./thumbnails.scss";
import { changeAsideInfo } from "./../../actions";

function Thumbnails({ list, mode, changeAsideInfo }) {
  const handleThumbClick = (id) => {
    if (mode === "home") {
      changeAsideInfo({ id });
    }
  };
  return (
    <ul className={thumbnails}>
      {list &&
        list.map((item) => (
          <li key={item.id} className={thumbnailListItem}>
            <button
              onClick={() => handleThumbClick(item.id)}
              className={thumbnailBtn}
            >
              <img className={thumbnailImg} src={item.image} alt="" />
            </button>
          </li>
        ))}
    </ul>
  );
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    changeAsideInfo: (payload) => dispatch(changeAsideInfo(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Thumbnails);
