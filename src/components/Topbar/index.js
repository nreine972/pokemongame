import React from "react";
import TopbarButton from "./../TopbarButton";
import logo from "url:./Pokemon-Logo.png";
import { topbar } from "./topbar.scss";

export default function Topbar() {
  return (
    <header className={topbar}>
      <img width="150" src={logo} alt="" />
      <TopbarButton />
    </header>
  );
}
