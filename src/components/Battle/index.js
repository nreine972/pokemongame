import React from "react";
import { connect } from "react-redux";
import Card from "../Card";
import BattleMenu from "../BattleMenu";
import {
  battleClose,
  battleOpen,
  battleComputerZone,
  battleUserZone,
  battleArena,
} from "./battle.scss";

function Battle({ ui, battle }) {
  return (
    <div className={ui.battle.enabled ? battleOpen : battleClose}>
      <div className={battleArena}>
        {battle.computer && battle.computer.active && (
          <div className={battleComputerZone}>
            <Card data={battle.computer.active} />
            <progress
              value={battle.computer.active.health}
              max="100"
            ></progress>
          </div>
        )}
        {battle.user && battle.user.active && (
          <div className={battleUserZone}>
            <Card data={battle.user.active} />
            <progress value={battle.user.active.health} max="100"></progress>
          </div>
        )}
      </div>
      <BattleMenu />
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });
export default connect(mapStateToProps)(Battle);
