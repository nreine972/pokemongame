import React from "react";
import { search } from "./search.scss";

export default function Search() {
  return (
    <div>
      <input className={search} type="text" />
    </div>
  );
}
