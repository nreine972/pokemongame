import React from "react";
import { connect } from "react-redux";
import {
  battleMenu,
  battleMenuList,
  battleMenuListItem,
  battleMenuButton,
  battleMenuContainer,
} from "./battlemenu.scss";

import { launchAttack } from "./../../actions";

function BattleMenu({ battle, launchAttack }) {
  const handleAttackClick = (attack) => {
    launchAttack({
      attack,
      attacker: battle.user.active,
      receiver: battle.computer.active,
      turn: "user",
    });
  };
  return (
    <div className={battleMenu}>
      <div className={battleMenuContainer}>
        {battle.user.active && (
          <ul className={battleMenuList}>
            {battle.user.active.selectedAttacks &&
              battle.user.active.selectedAttacks
                .filter((attack) => attack.category === "fast")
                .map((attack) => (
                  <li
                    key={`${battle.user.active}-${attack.name}`}
                    className={battleMenuListItem}
                  >
                    <button
                      onClick={() => {
                        handleAttackClick(attack);
                      }}
                      className={battleMenuButton}
                    >
                      {attack.name}
                    </button>
                  </li>
                ))}
          </ul>
        )}
      </div>
      <div className={battleMenuContainer}>
        {battle.user.active && (
          <ul className={battleMenuList}>
            {battle.user.active.selectedAttacks &&
              battle.user.active.selectedAttacks
                .filter((attack) => attack.category === "special")
                .map((attack) => (
                  <li
                    key={`${battle.user.active}-${attack.name}`}
                    className={battleMenuListItem}
                  >
                    <button
                      onClick={() => {
                        handleAttackClick(attack);
                      }}
                      className={battleMenuButton}
                    >
                      {attack.name}
                    </button>
                  </li>
                ))}
          </ul>
        )}
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({ ...state });
const mapDispatchToProps = (dispatch) => {
  return {
    launchAttack: (payload) => dispatch(launchAttack(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BattleMenu);
